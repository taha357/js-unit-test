const log = console.log;
const should = require('chai').should();
const expect = require('chai').expect;
const _ = require('lodash');

const { readCow } = require('./index');

describe('#Run readCow', () => {
    it('should be equal to "yo"', (done) => {
        readCow((err, content) => {
            content.should.equal('yo');
            done();
        });
    });
    it('should not have an error', (done) => {
        readCow((err, content) => {
            expect(err).to.not.exist;
            done();
        });
    });
});

